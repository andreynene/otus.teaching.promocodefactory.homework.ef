﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public static class LessonDatabaseContextFactory
{
    public static IServiceCollection ConfigureContext(this IServiceCollection services)
    {
        services.AddDbContext<LessonDatabaseContext>(optionsBuilder
            => optionsBuilder
                .UseSqlite());
        return services;
        
    }
    
    public class AppealDatabaseContextFactory: IDesignTimeDbContextFactory<LessonDatabaseContext>
    {
        public LessonDatabaseContext CreateDbContext(string[] args)
        {
            var migrationsAssemblyName = typeof(LessonDatabaseContext).GetTypeInfo().Assembly.GetName().Name;
            var optionsBuilder = new DbContextOptionsBuilder<LessonDatabaseContext>()
                .UseSqlite("Data Source=../LocalDatabase.db", sqlServerOptions => sqlServerOptions.MigrationsAssembly(migrationsAssemblyName));

            return new LessonDatabaseContext(optionsBuilder.Options);
        }
    }
}