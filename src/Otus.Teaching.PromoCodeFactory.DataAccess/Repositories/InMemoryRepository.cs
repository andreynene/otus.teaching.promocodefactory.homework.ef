﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> _data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            _data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync() => Task.FromResult(_data);

        public Task<T> GetByIdAsync(Guid id) => Task.FromResult(_data.FirstOrDefault(x => x.Id == id));

        public Task Add(T entity) =>
            AddRange(new[] { entity });

        public Task AddRange(IEnumerable<T> entities)
        {
            var newRecord = _data.ToList();
            newRecord.AddRange(entities);
            _data = newRecord;
            return Task.CompletedTask;
        }

        public Task Update(T entity)
        {
            var newRecord = _data.Where(x=>x.Id != entity.Id).ToList();
            newRecord.Add(entity);
            _data = newRecord;
            return Task.CompletedTask;
        }

        public Task RemoveRange(IEnumerable<Guid> records)
        {
            _data = _data.Where(x=>!records.Contains(x.Id));
            return Task.CompletedTask;
        }

        public Task DeleteByIdAsync(Guid id)
        {
            _data = _data.Where(x => x.Id != id);
            return Task.CompletedTask;
        }

        public Task<T> GetAsync(Guid id)
            => Task.FromResult(_data.FirstOrDefault(x => x.Id == id));
    }
}