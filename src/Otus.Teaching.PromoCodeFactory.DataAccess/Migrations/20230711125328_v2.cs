﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class v2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_base_entity_customer_CustomerId",
                table: "base_entity");

            migrationBuilder.DropForeignKey(
                name: "FK_base_entity_preference_PreferenceId",
                table: "base_entity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_base_entity",
                table: "base_entity");

            migrationBuilder.RenameTable(
                name: "base_entity",
                newName: "promo_code");

            migrationBuilder.RenameIndex(
                name: "IX_base_entity_PreferenceId",
                table: "promo_code",
                newName: "IX_promo_code_PreferenceId");

            migrationBuilder.RenameIndex(
                name: "IX_base_entity_CustomerId",
                table: "promo_code",
                newName: "IX_promo_code_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_promo_code",
                table: "promo_code",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_promo_code_customer_CustomerId",
                table: "promo_code",
                column: "CustomerId",
                principalTable: "customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_promo_code_preference_PreferenceId",
                table: "promo_code",
                column: "PreferenceId",
                principalTable: "preference",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_promo_code_customer_CustomerId",
                table: "promo_code");

            migrationBuilder.DropForeignKey(
                name: "FK_promo_code_preference_PreferenceId",
                table: "promo_code");

            migrationBuilder.DropPrimaryKey(
                name: "PK_promo_code",
                table: "promo_code");

            migrationBuilder.RenameTable(
                name: "promo_code",
                newName: "base_entity");

            migrationBuilder.RenameIndex(
                name: "IX_promo_code_PreferenceId",
                table: "base_entity",
                newName: "IX_base_entity_PreferenceId");

            migrationBuilder.RenameIndex(
                name: "IX_promo_code_CustomerId",
                table: "base_entity",
                newName: "IX_base_entity_CustomerId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_base_entity",
                table: "base_entity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_base_entity_customer_CustomerId",
                table: "base_entity",
                column: "CustomerId",
                principalTable: "customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_base_entity_preference_PreferenceId",
                table: "base_entity",
                column: "PreferenceId",
                principalTable: "preference",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
